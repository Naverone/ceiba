//
//  Network.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 12/9/21.
//

import Foundation

class Network<T: Codable> {
    
    func request(_ url: String, callBack: @escaping(T?,String) -> Void)/*, completion: (([T]?, NSError?) -> Void)?)*/{
        
        let url = URL(string: url)!

        var urlRequest = URLRequest(url: url, timeoutInterval: timeNetworkInterval)
        urlRequest.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: url) { (data, response , error) in
            if let _ = error {
                
                DispatchQueue.main.async {
                    callBack(nil, errorNetworking)
                }
                
            }
            else {
                
                guard let data = data else { return }
                
                let decoder = JSONDecoder()
                
                if let loaded = try? decoder.decode(T.self, from: data) {
                    
                    DispatchQueue.main.async {
                        callBack(loaded, "")
                    }
                }
                else {
                    DispatchQueue.main.async {
                        callBack(nil, "Error al decode json desde \(url).")
                    }
                }
                
            }
           
            
            
        }.resume()
    }
}
