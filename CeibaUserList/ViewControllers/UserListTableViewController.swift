//
//  UserListTableViewController.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 11/9/21.
//

import UIKit

class UserListTableViewController: UITableViewController {
    
    // MARK: - PROPERTIES
    private let dataSource = UserModel()
    private let dataSourcePost = PostModel()
    
    private var users: Array<UserData>?
    private var filteredUsers: Array<UserData> = []
    
    private var post: Array<Post>?
    
    private var user: UserData?
    
    //: Reference to managed object context
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //: Data for the table
    var usersCore: [User]?
    
    // MARK: - Computer porpertios para filtrar
    let searchController = UISearchController(searchResultsController: nil)

    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBarEmpty
    }
    
    // MARK: - Cicle of life
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        // 1
        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = searchBarPlaceHolder
        searchController.searchBar.tintColor = colorAccent
        searchController.searchBar.barTintColor = UIColor.white
        
        if let textfield = searchController.searchBar.value(forKey: "searchField") as? UITextField {
            //textfield.textColor = // Set text color
//            textfield.backgroundColor =  UIColor(named: )
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            
            textfield.tintColor = UIColor.black
          
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = UIColor.black
            }
            
        }
        // 4
        //        if let navigationbar = self.navigationController?.navigationBar {
        //            navigationbar.barTintColor = UIColor.blue
        //        }
        tableView.tableHeaderView = searchController.searchBar
        // 5
        definesPresentationContext = true
        
        fetchUserCore()
        
//        dataSourcePost.requestData(callBack: self.procesar2)
    }
    
    // MARK: - FUNCTIONS
    
    func fetchUserCore() {
        
        do {
            self.usersCore =  try context.fetch(User.fetchRequest())
            
            if self.usersCore!.isEmpty {
                dataSource.requestData(callBack: self.getUserData)
                Load.start()
            }
            else {
                
                self.users = []
                
                for userCore in self.usersCore! {
                    
                    let geo = geo(lat: userCore.addressBil.location.lat, lng: userCore.addressBil.location.lng)
                    
                    let address = address(street: userCore.addressBil.street, suite: userCore.addressBil.suite, city: userCore.addressBil.city, zipcode: userCore.addressBil.zipcode, geo: geo)
                    
                    let company = company(name: userCore.comp.name, catchPhrase: userCore.comp.catchPhrase, bs: userCore.comp.bs)
                    
                    let user = UserData(id: Int(userCore.id), name: userCore.name, username: userCore.userName, email: userCore.email, address: address, phone: userCore.phone, website: userCore.website, company: company)
                    
                    self.users?.append(user)
                }
                
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        catch {
            showAlert(title: alertTitle, message: errorCoreData)
        }
        
        
        
    }
    
    func getUserData(userData: Array<UserData>?, error: String) {
        
        if let users = userData {
//            self.users = user
            
            for i in users {
                
                //Create user
                let userCore = User(context: context)
                userCore.id = Int64(i.id)
                userCore.name = i.name
                userCore.userName = i.username
                userCore.email = i.email
                userCore.phone = i.phone
                userCore.website = i.website
                
                //Create address
                let addressCore = Address(context: context)
                
                addressCore.street = i.address.street
                addressCore.suite = i.address.suite
                addressCore.city = i.address.city
                addressCore.zipcode = i.address.zipcode
                
                
                
                //Create geo
                let geoCore = Geo(context: context)
                geoCore.lat = i.address.geo.lat
                geoCore.lng = i.address.geo.lng
                
                addressCore.location = geoCore
                userCore.addressBil = addressCore

                //Create company
                let companyCore = Company(context: context)
                companyCore.name = i.company.name
                companyCore.catchPhrase = i.company.catchPhrase
                companyCore.bs = i.company.bs
                
                userCore.comp = companyCore
                
                // save context
                
                try! context.save()
                
            }
            
            self.users = users
            self.tableView.reloadData()
            Load.stop()
        }
        else {
            self.showAlert(title: alertTitle, message: error)
        }
    }
    
    func getUserPost(postUser: Array<Post>?, error: String) {
        
        if let post = postUser {
            
            self.post = post
            self.performSegue(withIdentifier: "showUserPost", sender: nil)
            
        }
        else {
            self.showAlert(title: alertTitle, message: error)
        }
    }
    
    func filterContentForSearchText(_ searchText: String ) {
        
        filteredUsers = users!.filter { (Usuario: UserData) -> Bool in
            
            let words = Usuario.name.split(separator: " ")// +  beneficiary.username.split(separator: " ")
            for word in words {
                print(word)
                if  word.lowercased().starts(with: searchText.lowercased()) {
                    return true
                }
            }
            
            if Usuario.name.lowercased().contains(searchText.lowercased()){
                return true
            }
            
            return false
        }

        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        if self.users == nil {
            return 0
        }
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if isFiltering {
            return self.filteredUsers.count
        }
        
        return self.users?.count ?? 1
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let user: UserData
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as! UserListTableViewCell
        
        if isFiltering {
            user = self.filteredUsers[indexPath.row]
        } else {
            user = self.users![indexPath.row]
        }
        
        cell.setUser(user: user)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user: UserData
        
        if isFiltering {
            user = self.filteredUsers[indexPath.row]
        } else {
            user = self.users![indexPath.row]
        }
        
//         if let users = self.users {
            self.user = user
            let userId = user.id
            
            dataSourcePost.requestData( userId: userId,callBack: self.getUserPost)
            Load.start()
            
//        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if let destination = segue.destination as? UserPostViewController {
            destination.user = self.user
            destination.post = self.post
        }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UserListTableViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
        
    }
    
}
