//
//  UserPostViewController.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 12/9/21.
//

import UIKit

class UserPostViewController: UIViewController {

    // MARK: - PROPERTIES
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var tableview: UITableView!
    
    var user: UserData?
    var post: Array<Post>?
    
    // MARK: - Cicle of life
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        Load.stop()
        setupView()
    }
    
    // MARK: - FUNCIONS
    func setupView () {
        
        self.userName.text = user?.name
        self.userEmail.text = user?.email
        self.userPhone.text = user?.phone
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
}

// MARK: - Table view data source
extension UserPostViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if self.post == nil {
            return 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.post?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! UserPostTableViewCell
        
        guard let post = self.post else {
            return cell
        }
        
        cell.setPost(post: post[indexPath.row])
        
        return cell
    }
    
}
