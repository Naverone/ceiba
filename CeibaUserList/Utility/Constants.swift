//
//  Constants.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 13/9/21.
//

import Foundation
import UIKit

//text
let searchBarPlaceHolder: String = "Buscar Usuario"
let alertTitle: String = "Antención"
let errorCoreData: String = "Ocurrion un problema en coreData"
let errorNetworking: String = "Compruebe su conexión a Internet"

//COLOR
let colorAccent: UIColor = UIColor(named: "AccentColor") ?? .black

//time interval networking
let timeNetworkInterval: Double = 20.0
