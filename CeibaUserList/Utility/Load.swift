//
//  Load.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 12/9/21.
//

import Foundation
import UIKit

public class Load {
    
    internal static var load: UIActivityIndicatorView?
    
    public static var style: UIActivityIndicatorView.Style = .large
    public static var baseBackColor = UIColor(white: 0, alpha: 0.7)
    
    public static func start(style: UIActivityIndicatorView.Style = style, backColor: UIColor = baseBackColor, baseColor: UIColor = .white) {
        if load == nil, let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first {
            let frame = UIScreen.main.bounds

            load = UIActivityIndicatorView(frame: frame)
            load!.backgroundColor = backColor
            load!.style = style
            load?.color = baseColor
            window.addSubview(load!)
            load!.startAnimating()
            
        }
    }
    
    public static func stop() {
        if load != nil {
            load!.stopAnimating()
            load!.removeFromSuperview()
            load = nil
        }
    }
    
    public static func update() {
        if load != nil {
            stop()
            start()
        }
    }
    
}
