//
//  PostItem.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 12/9/21.
//

import Foundation


// MARK: - PostElement
struct PostElement: Codable {
    let userID, id: Int
    let title, body: String

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body
    }
}

typealias Post = PostElement


class PostModel: NSObject {
    
    func requestData<T: Codable>(userId id: Int, callBack: @escaping(T?, String) -> Void) {
        
        Network<T>().request("https://jsonplaceholder.typicode.com/posts?userId=\(id)", callBack: callBack)
    }

}
