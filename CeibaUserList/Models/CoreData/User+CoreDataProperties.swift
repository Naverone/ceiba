//
//  User+CoreDataProperties.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 13/9/21.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var id: Int64
    @NSManaged public var email: String
    @NSManaged public var userName: String
    @NSManaged public var name: String
    @NSManaged public var phone: String
    @NSManaged public var website: String
    @NSManaged public var addressBil: Address
    @NSManaged public var comp: Company

}

extension User : Identifiable {

}
