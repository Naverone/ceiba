//
//  Geo+CoreDataProperties.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 13/9/21.
//
//

import Foundation
import CoreData


extension Geo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Geo> {
        return NSFetchRequest<Geo>(entityName: "Geo")
    }

    @NSManaged public var lng: String
    @NSManaged public var lat: String
    @NSManaged public var addresBil: Address

}

extension Geo : Identifiable {

}
