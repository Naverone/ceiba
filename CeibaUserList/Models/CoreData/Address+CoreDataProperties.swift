//
//  Address+CoreDataProperties.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 13/9/21.
//
//

import Foundation
import CoreData


extension Address {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Address> {
        return NSFetchRequest<Address>(entityName: "Address")
    }

    @NSManaged public var street: String
    @NSManaged public var zipcode: String
    @NSManaged public var city: String
    @NSManaged public var suite: String
    @NSManaged public var location: Geo
    @NSManaged public var people: User

}

extension Address : Identifiable {

}
