//
//  User.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 11/9/21.
//

import Foundation
import UIKit

// MARK: - UserElement
struct UserElement: Identifiable, Codable {
//    weak var delegate : UserModelDelegate?
    
    let id: Int
    let name, username, email: String
    let address: address
    let phone, website: String
    let company: company
    
}

// MARK: - Address
struct address: Codable {
    let street, suite, city, zipcode: String
    let geo: geo
}

// MARK: - Geo
struct geo: Codable {
    let lat, lng: String
}

// MARK: - Company
struct company: Codable {
    let name, catchPhrase, bs: String
}

typealias UserData = UserElement

class UserModel: NSObject {
    
    func requestData<T: Codable>(callBack: @escaping(T?, String) -> Void) {
        
        Network<T>().request("https://jsonplaceholder.typicode.com/users", callBack: callBack)
    }
}

