//
//  UserPostTableViewCell.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 12/9/21.
//

import UIKit

class UserPostTableViewCell: UITableViewCell {

    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postBody: UILabel!

    func setPost(post: Post){
        self.postTitle.text = "\(post.title)"
        self.postBody.text = "\(post.body)"
    }
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
