//
//  UserListTableViewCell.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 12/9/21.
//

import UIKit

class UserListTableViewCell: UITableViewCell {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhone: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userDetails: UIButton!
    
    func setUser(user: UserData){
        self.userName.text = "\(user.name)"
        self.userPhone.text = "\(user.phone)"
        self.userEmail.text = "\(user.email)"
    }
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
