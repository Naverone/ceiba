//
//  UIView.swift
//  CeibaUserList
//
//  Created by IT Security Solutions  on 12/9/21.
//

import Foundation
import UIKit


//@IBDesignable extension UIView {
//    @IBInspectable var cornerRadius: CGFloat {
//        get { return layer.cornerRadius }
//        set {
//              layer.cornerRadius = newValue
//
//              // If masksToBounds is true, subviews will be
//              // clipped to the rounded corners.
//              layer.masksToBounds = (newValue > 0)
//        }
//    }
//}

@IBDesignable
class SubViewAtributes: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable var maskToBounds: Bool = false {
        didSet {
            layer.masksToBounds = maskToBounds ? true : false
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        }
        set {
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowRadius: Double {
        get {
            return Double(self.layer.shadowRadius)
        }
        set {
            self.layer.shadowRadius = CGFloat(newValue)
        }
    }
    
    func setupView() {
        
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = maskToBounds

    }
}
