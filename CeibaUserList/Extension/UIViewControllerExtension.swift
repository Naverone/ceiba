//
//  UIViewControllerExtension.swift
//  CeibaUserList
//
//  Created by Jacobo Reyes  on 12/9/21.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlert(title:  String, message: String){

        if let currentAlert = self.presentedViewController as? UIAlertController {

            currentAlert.dismiss(animated: true, completion: nil)
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ACEPTAR", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
